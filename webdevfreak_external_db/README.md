# Connect to external database module.

### Description

This module allows to make a successful connection to an external database. 

### Requirements

- Drupal ^8.8.0 || ^9

### Installation

Install as normal Drupal module is installed i.e 

a) By going to '/admin/modules'

b) Via drush i.e 'drush en webdevfreak_external_db'

### Configuration

Search "$databases['default']['default']" in your settings.php file. 

Add following code below above block soon after where array ends i.e 

(First test external db connection via simply PHP script before using below)

$databases['external']['default'] = array (
  'database' => 'your_external_database_name',
  'username' => 'root',
  'password' => 'password',
  'prefix' => '',
  // 'host' => 'localhost',
  'host' => '127.0.0.1',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

You can update database query from the controller file and then can test the 
output by going to '/test-db-connection' in your browser.