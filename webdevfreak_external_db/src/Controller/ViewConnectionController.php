<?php

/**
 * @file
 * Contains \Drupal\webdevfreak_external_db\Controller\ViewConnectionController.
 */

namespace Drupal\webdevfreak_external_db\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;

class ViewConnectionController extends ControllerBase {

  public function check_external_connection(){    

    // Connect to external database.
    Database::setActiveConnection('external');

    // Run query on external database.
    $results = Database::getConnection()->query('SELECT count(aid) FROM actions')->fetchAll();    
    dpm($results);

    // Connect back to default database.
    Database::setActiveConnection();

    return $results;
  }

}
